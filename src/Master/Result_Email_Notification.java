package Master;

import java.io.File;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Result_Email_Notification extends DataCall{
	public static String to;
@BeforeTest
public void BeforeSendEmail() throws Exception
{
	emailid();
}
@Test 
public void SendMail() throws Exception 
{
      // Recipient's email ID needs to be mentioned.
	  
      //String to = "vishal.mirani@techholding.co";
		//emailid();
		to = sh.getCell(7,1).getContents();
      // Sender's email ID needs to be mentioned
      
      String from = "techholdingqa@gmail.com";

      final String username = "techholdingqa@gmail.com";//change accordingly
      final String password = "Tech@123";//change accordingly

      // Assuming you are sending email through relay.jangosmtp.net
      String host = "smtp.gmail.com";

      Properties props = new Properties();
      props.put("mail.smtp.auth", "true");
      /*props.put("mail.smtp.socketFactory.port","465");
      props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");*/
      props.put("mail.smtp.starttls.enable", "true");
      props.put("mail.smtp.host", host);
      props.put("mail.smtp.port", "587");

      // Get the Session object.
      Session session = Session.getInstance(props, new javax.mail.Authenticator()
      {
            protected PasswordAuthentication getPasswordAuthentication()
            {
            return new PasswordAuthentication(username, password);
            }
      });

      try {
         // Create a default MimeMessage object.
         Message message = new MimeMessage(session);

         // Set From: header field of the header.
         message.setFrom(new InternetAddress(from));

         // Set To: header field of the header.
         message.setRecipients(Message.RecipientType.TO,
            InternetAddress.parse(to));

         // Set Subject: header field
         message.setSubject("90 Plus Wine Test Automation Result");

         // Create the message part
         BodyPart messageBodyPart = new MimeBodyPart();

         // Now set the actual message
         messageBodyPart.setText("Please find attached 90 Plus Wine Test Automation Result.");

         // Create a multipar message
         Multipart multipart = new MimeMultipart();

         // Set text message part
         multipart.addBodyPart(messageBodyPart);

         // Part two is attachment
         messageBodyPart = new MimeBodyPart();
         
         File f = new File(System.getProperty("user.dir"));
         File[] attachments = f.listFiles();

         // Part two is attachment
         for( int i = 0; i < attachments.length; i++ ) {
             if (attachments[i].isFile() && attachments[i].getName().startsWith("90PlusWineTestData.xls")) {
                 messageBodyPart = new MimeBodyPart();
                 DataSource Source =new FileDataSource(attachments[i]);
                 messageBodyPart.setDataHandler(new DataHandler(Source));
                 messageBodyPart.setFileName(attachments[i].getName());
                 messageBodyPart.setDisposition(MimeBodyPart.INLINE);
                 multipart.addBodyPart(messageBodyPart);
             }
         }
         File f1 = new File(System.getProperty("user.dir")+ "//ScreenShot");
         File[] attachments1 = f1.listFiles();
         if (f1.isDirectory()){
         // Part two is attachment
         for( int i = 0; i < attachments1.length; i++ )
         {
             if (attachments1[i].isFile() && attachments1[i].getName().contains(".png") && attachments1.length>0)  {
                 messageBodyPart = new MimeBodyPart();
                 DataSource Source =new FileDataSource(attachments1[i]);
                 messageBodyPart.setDataHandler(new DataHandler(Source));
                 messageBodyPart.setFileName(attachments1[i].getName());
                 messageBodyPart.setDisposition(MimeBodyPart.INLINE);
                 multipart.addBodyPart(messageBodyPart);
             }
         }
         }
         // Send the complete message parts
         message.setContent(multipart);

         // Send message
         Transport.send(message);
      } 
      catch (MessagingException e)
      {
         throw new RuntimeException(e);
      }
   }
@AfterTest
public void AfterSendEMail()
{
    System.out.println(to);
	System.out.println("Sent message successfully....");

}
}
