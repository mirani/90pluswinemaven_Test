package Master;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;


public class DataCallchrome {

	public WebDriver driver;
	public static String username;
	public static String password;
	public static String fullname;
	public static String mobile;
	public static String firstname;
	public static String lastname;
	public static String email;
	public static String center;
	public static String TestResult;
	public static String TestReason;
	public static String todaysdate;
	public static String dateemail;
	public static String datemobile;
	public static String toemail;
	public static String contactcomment;
	public static int totalNoOfRows;
	public static int row;
	public static int row1;
	public static int col;
	public static int col1;
	public static String testresult;
	public static String finalresult;
	public static String final_result;
	public static Sheet sh;
	public static Sheet sh1;
	public static Sheet sh2;
	public static Sheet sh3;
	public static Sheet sh4;
	private Logger logger;
	private Properties obj;
	private WebDriverWait wait;
	public static String price_range;
	public static String bottle_ship;
	public static String time_per_year;
	public static String streetaddress1;
	public static String streetaddress2;
	public static String city;
	public static String state;
	public static String zip;
	public static String cardname;
	public static String cardnumber;
	public static String expdate;
	public static String cvv;
	public static String zipcode;
	public static String screenshotname;
	public static String Red1;
	public static String White1;
	public static String Rose1;
	public static String Sparkling1;
	public static String RedMeats2;
	public static String Poultry2;
	public static String Seafood2;
	public static String French2;
	public static String Italian2;
	public static String Mexican2;
	public static String Japanese2;
	public static String Chinese2;
	public static String MiddleEastern2;
	public static String Indian2;
	public static String Coffee3;
	public static String Tea3;
	public static String FruitJuices3;
	public static String VegetableJuices3;
	public static String Vodka3;
	public static String Tequila3;
	public static String Beer3;
	public static String Whiskey3;
	public static String Rum3;
	public static String MixedDrink3;
	public static String Dark4;
	public static String Milk4;
	public static String White4;
	public static String Apple5;
	public static String Peach5;
	public static String Orange5;
	public static String Banana5;
	public static String Grapes5;
	public static String Berries5;
	public static String Rose6;
	public static String WetForest6;
	public static String Beach6;
	public static String Leather6;
	public static String Berries6;
	public static String Coffee6;
	public static String Citrus6;
	public static String Spicy7;
	public static String Salty7;
	public static String Sour7;
	public static String Sweet7;
	public static String GardenHerbs7;
	public static String Fruity8;
	public static String Dry8;
	public static String Spicy8;
	public static String LightBody8;
	public static String MediumBody8;
	public static String FullBody8;
	public static String Comment9;

//---------------------------------Taking Screen Shot---------------------------------------------------	
	public void getscreenshot() throws Exception 
	{
	        File scrFile = ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.FILE);
	     
	        FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") +"//ScreenShot//" +screenshotname + ".png"));
	}

//---------------------------------Write Result in Excel Sheet--------------------------------------------
	public void setregisterresult() throws Exception 
	{
		//Workbook  book = Workbook.getWorkbook(new File(System.getProperty("user.dir") + "\\TestData.xls"));
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet sheetToEdit = workbook.getSheet("Register");
	    Label l1 = new Label(5, row, TestResult);
	    sheetToEdit.addCell(l1);
	    Label l2 = new Label(6, row, TestReason);
	    sheetToEdit.addCell(l2);
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();		
	}
	public void setregisterdetails() throws Exception 
	{
		//Workbook  book = Workbook.getWorkbook(new File(System.getProperty("user.dir") + "\\TestData.xls"));
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet sheetToEdit = workbook.getSheet("Register");
	    WritableSheet sheetToEdit1 = workbook.getSheet("StartTheQuiz");
	    WritableSheet sheetToEdit2 = workbook.getSheet("Shipping");
	    
	    Label l7 = new Label(1, row, "");
	    sheetToEdit.addCell(l7);
	    
	    Label l8 = new Label(2, row, "");
	    sheetToEdit.addCell(l8);
	    
	    Label l1 = new Label(1, row, dateemail);
	    sheetToEdit.addCell(l1);
	    
	    Label l2 = new Label(2, row, datemobile);
	    sheetToEdit.addCell(l2);
	    
	    Label l3 = new Label(0, row, dateemail);
	    sheetToEdit1.addCell(l3);
	    
	    Label l4 = new Label(0, row, dateemail);
	    sheetToEdit2.addCell(l4);
	    
	    Label l5 = new Label(5, row, "");
	    sheetToEdit.addCell(l5);
	    
	    Label l6 = new Label(6, row, "");
	    sheetToEdit.addCell(l6);
	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();		
	}
//-----------------------------------Write Result in CSV File---------------------------------------
	
    public void csvresult() throws Exception, IOException
    {
        /*CsvWriter csvOutput = new CsvWriter(new FileWriter(System.getProperty("user.dir") + "//finalresult.csv", true),' ');
        csvOutput.write(finalresult);
        csvOutput.endRecord();              
        csvOutput.close();*/
        OutputStream fileStream = new BufferedOutputStream(new FileOutputStream(System.getProperty("user.dir") + "//finalresult.csv"));
        Writer outStreamWriter = new OutputStreamWriter(fileStream, StandardCharsets.UTF_8);
        @SuppressWarnings("resource")
		BufferedWriter buffWriter = new BufferedWriter(outStreamWriter);
        buffWriter.append(finalresult);
        buffWriter.flush();
    }
//-----------------------------------Driver Setup-----------------------------------------------------	
	public void driverset() throws Exception 
	{
		  System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//chromedriver"); 
		  System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");
		  setObj(new Properties());					
		  FileInputStream objfile = new FileInputStream(System.getProperty("user.dir")+"//application.properties");
		  getObj().load(objfile);
		  
		  setLogger(Logger.getLogger("Login"));
	      PropertyConfigurator.configure(System.getProperty("user.dir")+ "//Log4j.properties"); 
	      
	      ChromeOptions options = new ChromeOptions();
		  options.setHeadless(true);
		  //options.setBinary("/usr/bin/chromedriver");
		  options.addArguments("--start-maximized");
		  options.addArguments(System.getProperty("user.dir") + "//chromedriver");
		  options.addArguments("--disable-extensions");
		  options.addArguments("--headless");
		  options.addArguments("--disable-gpu");
		  options.addArguments("--window-size=1440,900");
		  options.addArguments("--no-sandbox");
		  options.addArguments("--browsertime.xvfb");

		  setDriver(new ChromeDriver(options));
	      driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);  
	      setWait(new WebDriverWait(getDriver(), 90));
	      getDriver().get(getObj().getProperty("URL"));
	      //getDriver().manage().window().maximize();
	      getLogger().info("Browser Opened");
	      String FilePath = System.getProperty("user.dir") + "//90PlusWineTestData.xls";
	      FileInputStream fs = new FileInputStream(FilePath);
	      Workbook wb = Workbook.getWorkbook(fs);
	      sh = wb.getSheet("Register");
	      sh1 = wb.getSheet("StartTheQuiz");
	      sh2 = wb.getSheet("Shipping");
	      sh3 = wb.getSheet("Validation_Result");
	      sh4 = wb.getSheet("Final_Result");
	      
	      SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyHHmm");  
	      Date date = new Date(); 
	      todaysdate = formatter.format(date);
	}
	public void deleteresultfile() throws Exception
	{
		File f1 = new File(System.getProperty("user.dir")+ "//finalresult.csv");
		if(f1.isFile())
		{
			f1.delete();
		}
	}
	public void deletescreenshot() throws Exception
	{
		File f1 = new File(System.getProperty("user.dir")+ "//ScreenShot");
		for(File file: f1.listFiles()) 
		if (!file.isDirectory()) 
		file.delete();
	}
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebDriverWait getWait() {
		return wait;
	}

	public void setWait(WebDriverWait wait) {
		this.wait = wait;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Properties getObj() {
		return obj;
	}

	public void setObj(Properties obj) {
		this.obj = obj;
	}
	public void registerData() throws Exception
	{
		fullname = sh.getCell(0,row).getContents();
		password = sh.getCell(3,row).getContents();
	}
	public void emailid() throws Exception
	{
		String FilePath = System.getProperty("user.dir") + "//90PlusWineTestData.xls";
	    FileInputStream fs = new FileInputStream(FilePath);
	    Workbook wb = Workbook.getWorkbook(fs);
	    sh = wb.getSheet("Register");
	}
	public void QuizQuestion() throws Exception
	{
		email = sh1.getCell(0,row).getContents();
		password = sh1.getCell(1,row).getContents();
		Red1 = sh1.getCell(2,row).getContents();
		White1 = sh1.getCell(3,row).getContents();
		Rose1 = sh1.getCell(4,row).getContents();
		Sparkling1 = sh1.getCell(5,row).getContents();
		RedMeats2 = sh1.getCell(6,row).getContents();
		Poultry2 = sh1.getCell(7,row).getContents();
		Seafood2 = sh1.getCell(8,row).getContents();
		French2 = sh1.getCell(9,row).getContents();
		Italian2 = sh1.getCell(10,row).getContents();
		Mexican2 = sh1.getCell(11,row).getContents();
		Japanese2 = sh1.getCell(12,row).getContents();
		Chinese2 = sh1.getCell(13,row).getContents();
		MiddleEastern2 = sh1.getCell(14,row).getContents();
		Indian2 = sh1.getCell(15,row).getContents();
		Coffee3 = sh1.getCell(16,row).getContents();
		Tea3 = sh1.getCell(17,row).getContents();
		FruitJuices3 = sh1.getCell(18,row).getContents();
		VegetableJuices3 = sh1.getCell(19,row).getContents();
		Vodka3 = sh1.getCell(20,row).getContents();
		Tequila3 = sh1.getCell(21,row).getContents();
		Beer3 = sh1.getCell(22,row).getContents();
		Whiskey3 = sh1.getCell(23,row).getContents();
		Rum3 = sh1.getCell(24,row).getContents();
		MixedDrink3 = sh1.getCell(25,row).getContents();
		Dark4 = sh1.getCell(26,row).getContents();
		Milk4 = sh1.getCell(27,row).getContents();
		White4 = sh1.getCell(28,row).getContents();
		Apple5 = sh1.getCell(29,row).getContents();
		Peach5 = sh1.getCell(30,row).getContents();
		Orange5 = sh1.getCell(31,row).getContents();
		Banana5 = sh1.getCell(32,row).getContents();
		Grapes5 = sh1.getCell(33,row).getContents();
		Berries5 = sh1.getCell(34,row).getContents();
		Rose6 = sh1.getCell(35,row).getContents();
		WetForest6 = sh1.getCell(36,row).getContents();
		Beach6 = sh1.getCell(37,row).getContents();
		Leather6 = sh1.getCell(38,row).getContents();
		Berries6 = sh1.getCell(39,row).getContents();
		Coffee6 = sh1.getCell(40,row).getContents();
		Citrus6 = sh1.getCell(41,row).getContents();
		Spicy7 = sh1.getCell(42,row).getContents();
		Salty7 = sh1.getCell(43,row).getContents();
		Sour7 = sh1.getCell(44,row).getContents();
		Sweet7 = sh1.getCell(45,row).getContents();
		GardenHerbs7 = sh1.getCell(46,row).getContents();
		Fruity8 = sh1.getCell(47,row).getContents();
		Dry8 = sh1.getCell(48,row).getContents();
		Spicy8 = sh1.getCell(49,row).getContents();
		LightBody8 = sh1.getCell(50,row).getContents();
		MediumBody8 = sh1.getCell(51,row).getContents();
		FullBody8 = sh1.getCell(52,row).getContents();
		Comment9 = sh1.getCell(53,row).getContents();
	}
	
	public void shippingData() throws Exception
	{
		email = sh2.getCell(0,row).getContents();
		password = sh2.getCell(1,row).getContents();
		price_range = sh2.getCell(2,row).getContents();
		bottle_ship = sh2.getCell(3,row).getContents();
		time_per_year = sh2.getCell(4,row).getContents();
		streetaddress1 = sh2.getCell(5,row).getContents();
		streetaddress2 = sh2.getCell(6,row).getContents();
		city = sh2.getCell(6,row).getContents();
		state = sh2.getCell(8,row).getContents();
		zip = sh2.getCell(9,row).getContents();
		cardname = sh2.getCell(10,row).getContents();
		cardnumber = sh2.getCell(11,row).getContents();
		expdate = sh2.getCell(12,row).getContents();
		cvv = sh2.getCell(13,row).getContents();
		zipcode = sh2.getCell(14,row).getContents();
	}
	public void validation_result() throws Exception
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Validation_Result");
	    
	    Label l1 = new Label(col,row,testresult);
	    resultsheet.addCell(l1);
	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_rv_clear() throws Exception
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Final_Result");
	    
	    Label l1 = new Label(0,1,"Not Run");
	    resultsheet.addCell(l1);	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_qv_clear() throws Exception
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Final_Result");
	    
	    Label l1 = new Label(1,1,"Not Run");
	    resultsheet.addCell(l1);	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_sv_clear() throws Exception
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Final_Result");
	    
	    Label l1 = new Label(2,1,"Not Run");
	    resultsheet.addCell(l1);	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_rc_clear() throws Exception
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Final_Result");
	    
	    Label l1 = new Label(3,1,"Not Run");
	    resultsheet.addCell(l1);	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_register_validation() throws Exception
	{
		String FilePath = System.getProperty("user.dir") + "//90PlusWineTestData.xls";
	    FileInputStream fs = new FileInputStream(FilePath);
	    Workbook wb = Workbook.getWorkbook(fs);  
		sh3 = wb.getSheet("Validation_Result");

		for (int row = 3; row < 13; row++) {
			final_result = sh3.getCell(2,row).getContents();
			if((final_result).contentEquals("Fail"))
			{
				
				col1 = 0;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
			else if ((final_result).contentEquals("Pass"))
			{
				col1 = 0;
				row1 = 1;	
				final_result = "Pass";
				this.final_result();				
			}
			else
			{
				col1 = 0;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
		}
	}
	public void final_result() throws Exception
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Final_Result");
	    
	    Label l1 = new Label(col1,row1,final_result);
	    resultsheet.addCell(l1);
	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_csv_result() throws Exception
	{
		String FilePath = System.getProperty("user.dir") + "//90PlusWineTestData.xls";
	    FileInputStream fs = new FileInputStream(FilePath);
	    Workbook wb = Workbook.getWorkbook(fs);  
		sh3 = wb.getSheet("Final_Result");
		for (col = 0; col < 4; col++) {
			final_result = sh3.getCell(col,1).getContents();
			System.out.println(final_result);
			if((final_result).contentEquals("Fail"))
			{
				finalresult = "Fail";
				break;
			}
			else if((final_result).contentEquals("Pass"))
			{
				finalresult = "Pass";
			}
			else
			{
				finalresult = "Fail";
				break;
			}
		}
		if(finalresult.contentEquals("Pass"))
		{
			finalresult = "Pass";
			csvresult();
		}
		else
		{
			finalresult = "Fail";
			csvresult();
		}

	}
	public void final_shipping_validation() throws Exception
	{
		String FilePath = System.getProperty("user.dir") + "//90PlusWineTestData.xls";
	    FileInputStream fs = new FileInputStream(FilePath);
	    Workbook wb = Workbook.getWorkbook(fs);  
		sh4 = wb.getSheet("Validation_Result");
		for (int row = 3; row < 18; row++) {
			final_result = sh4.getCell(6,row).getContents();
			if((final_result).contentEquals("Fail"))
			{
				col1 = 2;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
			else if ((final_result).contentEquals("Pass"))
			{
				col1 = 2;
				row1 = 1;
				final_result = "Pass";
				this.final_result();	
			}
			else
			{
				col1 = 2;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
		}
	}
	public void register_result_clear() throws Exception
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Validation_Result");
	    
	    Label l1 = new Label(2,3,"");
	    resultsheet.addCell(l1);
	    Label l2 = new Label(2,4,"");
	    resultsheet.addCell(l2);
	    Label l3 = new Label(2,5,"");
	    resultsheet.addCell(l3);
	    Label l4 = new Label(2,6,"");
	    resultsheet.addCell(l4);
	    Label l5 = new Label(2,7,"");
	    resultsheet.addCell(l5);
	    Label l6 = new Label(2,8,"");
	    resultsheet.addCell(l6);
	    Label l7 = new Label(2,9,"");
	    resultsheet.addCell(l7);
	    Label l8 = new Label(2,10,"");
	    resultsheet.addCell(l8);
	    Label l9 = new Label(2,11,"");
	    resultsheet.addCell(l9);
	    Label l10 = new Label(2,12,"");
	    resultsheet.addCell(l10);
	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void shipment_result_clear() throws Exception
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Validation_Result");
	    
	    Label l1 = new Label(6,3,"");
	    resultsheet.addCell(l1);
	    Label l2 = new Label(6,4,"");
	    resultsheet.addCell(l2);
	    Label l3 = new Label(6,5,"");
	    resultsheet.addCell(l3);
	    Label l4 = new Label(6,6,"");
	    resultsheet.addCell(l4);
	    Label l5 = new Label(6,7,"");
	    resultsheet.addCell(l5);
	    Label l6 = new Label(6,8,"");
	    resultsheet.addCell(l6);
	    Label l7 = new Label(6,9,"");
	    resultsheet.addCell(l7);
	    Label l8 = new Label(6,10,"");
	    resultsheet.addCell(l8);
	    Label l9 = new Label(6,11,"");
	    resultsheet.addCell(l9);
	    Label l10 = new Label(6,12,"");
	    resultsheet.addCell(l10);
	    Label l11 = new Label(6,13,"");
	    resultsheet.addCell(l11);
	    Label l12 = new Label(6,14,"");
	    resultsheet.addCell(l12);
	    Label l13 = new Label(6,15,"");
	    resultsheet.addCell(l13);
	    Label l14 = new Label(6,16,"");
	    resultsheet.addCell(l14);
	    Label l15 = new Label(6,17,"");
	    resultsheet.addCell(l15);
	    Label l16 = new Label(6,18,"");
	    resultsheet.addCell(l16);
	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	
}
