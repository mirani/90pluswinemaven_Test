package Master;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Final_Result extends DataCall{
@BeforeTest
public void BeforeResult() throws Exception,InterruptedException
{
	System.out.println("Now Writing Final Result");
}
@Test 
public void FinalResult() throws Exception,InterruptedException 
{
	deleteresultfile();
	final_register_validation();
	final_shipping_validation();
	final_contact_validation();
	final_csv_result();
}
@AfterTest
public void AfterResult() throws InterruptedException
{
	System.out.println("Final Result Written successfully....");

}
}
