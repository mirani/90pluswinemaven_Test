package Login;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import Master.DataCall;
import Master.Retry;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
public class Login_FP_Validation extends DataCall {
	public class validation extends Login_Master {
@BeforeTest
public void befortest() throws Exception {
		driverset();
}	
@Test (priority = 1, retryAnalyzer = Retry.class)
public void blank_login_data() throws Exception, Throwable,InterruptedException {
	
			final_lv_clear();
			final_fpv_clear();
			Thread.sleep(2000);
			login_button();
			Thread.sleep(1000);
			login_link();
			Thread.sleep(1000);
			login_now_button();			
			try
			{
				String blankemail = getDriver().findElement(By.xpath(getObj().getProperty("blank_email"))).getText();
	
				if(blankemail.equals("This field is required."))
				{
					getLogger().info("Require Email Validation Pass in Login Module");
					col = 14;
					row = 3;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Require Email Validation Fail in Login Module");
					col = 14;
					row = 3;
					testresult = "Fail";
					validation_result();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Require Email Validation Fail in Login Module due to Object not found");
				col = 14;
				row = 3;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				String blankpassword = getDriver().findElement(By.xpath(getObj().getProperty("blank_password"))).getText();
	
				if(blankpassword.equals("This field is required."))
				{
					getLogger().info("Require Password Validation Pass in Login Module");
					col = 14;
					row = 4;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Require Password Validation Fail in Login Module");
					col = 14;
					row = 4;
					testresult = "Fail";
					validation_result();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Require Password Validation Fail due to Object not found in Login Module");
				col = 14;
				row = 4;
				testresult = "Fail";
				validation_result();
			}
}
@Test (priority = 2)
public void invalid_email() throws Exception, Throwable {
	
			email = "test";
			useremailaddress();
			login_now_button();
	
			try
			{			
				String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("invalid_email"))).getText();

				if(emailinvalid.equals("Email address is invalid"))
				{
					getLogger().info("Email Validation Pass in Login Module");
					col = 14;
					row = 5;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Email Validation Fail in Login Module");
					col = 14;
					row = 5;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Email Validation Fail due to Object not found in Module");
				col = 14;
				row = 5;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}	
}

@Test (priority = 3)
public void invalid_password() throws Exception, Throwable {
	
			email = "Good2go@gmail.com";
			password = "1234";
			useremailaddress();
			userpassword();
			login_now_button();
	
			try
			{
				String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("invalid_password"))).getText();
		
				if(emailinvalid.equals("Your password must be a minimum of 6 characters."))
				{
					getLogger().info("Password Validation Pass in Login Module");
					col = 14;
					row = 6;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Password Validation Fail in Login Module");
					col = 14;
					row = 6;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Password Validation Fail due to Object not found in Login Module");
				col = 14;
				row = 6;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 4)
public void login_usr_not_found() throws Exception, Throwable {
	
			email = todaysdate + "@mailinator.com";
			password = "123456";
			useremailaddress();
			userpassword();
			login_now_button();
			Thread.sleep(2000);
	
			try
			{		
				String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("User_not_found"))).getText();

				if(emailinvalid.equals("User not found"))
				{
					getLogger().info("User Not Available Validation Pass in Login Module");
					col = 14;
					row = 7;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("User Not Available Validation Fail in Login Module");
					col = 14;
					row = 7;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			}
			catch(Exception e)
			{
				getLogger().info("User Not Available Validation Fail due to Object not found in Login Module");
				col = 14;
				row = 7;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 5)
public void forgot_password_email_blank() throws Exception, Throwable {
	
			Thread.sleep(1000);
			forgot_pwd_link();
			Thread.sleep(1000);
			reset_pwd_button();
	
			try
			{
				String blankemail = getDriver().findElement(By.xpath(getObj().getProperty("blank_email"))).getText();

				if(blankemail.equals("This field is required."))
				{
					getLogger().info("Require Email Validation Pass in Forgot Password Module");
					col = 14;
					row = 17;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Require Email Validation Fail in Forgot Password Module");
					col = 14;
					row = 17;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Require Email Validation Fail in Forgot Password Module due to Object not found");
				col = 14;
				row = 17;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}

@Test (priority = 6)
public void forgot_password_email_invalid() throws Exception, Throwable {
	
			Thread.sleep(1000);
			email = "test";
			useremailaddress();
			reset_pwd_button();
	
			try
			{		
				String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("invalid_email"))).getText();

				if(emailinvalid.equals("Email address is invalid"))
				{
					getLogger().info("Email Validation Pass in Forgot Password Module");
					col = 14;
					row = 18;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Email Validation Fail in Forgot Password Module");
					col = 14;
					row = 18;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Email Validation Fail due to Object not found in Forgot Password Module");
				col = 14;
				row = 18;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 7)
public void forgot_password_user_not_found() throws Exception, Throwable {
	
			Thread.sleep(1000);
			email = todaysdate + "@mailinator.com";
			useremailaddress();
			reset_pwd_button();
			Thread.sleep(1000);
	
			try
			{		
				String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("User_not_found"))).getText();

				if(emailinvalid.equals("User not found"))
				{
					getLogger().info("User Not Available Validation Pass in Forgot Password Module");
					col = 14;
					row = 19;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("User Not Available Validation Fail in Forgot Password Module");
					col = 14;
					row = 19;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			}
			catch(Exception e)
			{
				getLogger().info("User Not Available Validation Fail due to Object not found in Forgot Password Module");
				col = 14;
				row = 19;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@AfterTest
public void aftertest() {
   getDriver().quit();
		}
	}
}