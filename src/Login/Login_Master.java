package Login;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import Master.DataCall;

public class Login_Master extends DataCall {

		public void login_button() throws Exception 
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login_home"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login_home"))).click();
				getLogger().info("Login Button Clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Login Button not load.";
				getLogger().info(TestReason);
				screenshotname = "Login_Button_Link_Not_Load";
				getscreenshot();
				Assert.fail();
			}
		}
		public void login_link() throws Exception 
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login_link"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login_link"))).click();
				getLogger().info("Login Linked Clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Login link not load properly.";
				getLogger().info(TestReason);
				screenshotname = "Login_Link_Not_Load";
				getscreenshot();		
			}
		}
		public void login_now_button() throws Exception 
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login_now_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login_now_button"))).click();
				getLogger().info("Login Now Button Clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Login Now Button not load properly.";
				getLogger().info(TestReason);
				screenshotname = "Login_Now_Button_Not_Load";
				getscreenshot();		
			}
		}
		public void forgot_pwd_link() throws Exception 
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Forgot_pass_link"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Forgot_pass_link"))).click();
				getLogger().info("Forgot Password Link Clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Forgot Password Link Not Found";
				getLogger().info(TestReason);
				screenshotname = "Forgot_Password_Link_Not_Found";
				getscreenshot();
			}
		}
		public void reset_pwd_button() throws Exception 
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Reset_pass_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Reset_pass_button"))).click();
				getLogger().info("Reset Password Button Clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Reset Password Button not load properly.";
				getLogger().info(TestReason);
				screenshotname = "Reset_Password_Button_Not_Load";
				getscreenshot();		
			}
		}
		public void useremailaddress() throws Exception
		{
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("email"))));
				getDriver().findElement(By.xpath(getObj().getProperty("email"))).clear();
				Thread.sleep(1000);	
				getDriver().findElement(By.xpath(getObj().getProperty("email"))).sendKeys(email);
				getLogger().info("Email Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Email field not found";
				getLogger().info(TestReason);
				screenshotname = "Email_Field_Not_Found";
				getscreenshot();
			}	
		}
		public void userpassword() throws Exception
		{
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("password"))));
				getDriver().findElement(By.xpath(getObj().getProperty("password"))).clear();
				Thread.sleep(1000);
				getDriver().findElement(By.xpath(getObj().getProperty("password"))).sendKeys(password);
				getLogger().info("Password Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Password_Field_Not_Found";
				getscreenshot();
			}
		}
	
}
