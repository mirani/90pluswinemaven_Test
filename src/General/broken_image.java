package General;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Master.DataCall;

public class broken_image extends DataCall {
	private int invalidImageCount;

@BeforeTest
public void befortest() throws Exception {
			driverset();

}
@Test		
public void first() throws Exception, Throwable {
	Thread.sleep(5000);
	try {
		invalidImageCount = 0;
		List<WebElement> imagesList = driver.findElements(By.tagName("img"));
		System.out.println("Total no. of images are " + imagesList.size());
		for (WebElement imgElement : imagesList) {
			String ImageUrl=imgElement.getAttribute("src");
		     System.out.println(ImageUrl);
			if (imgElement != null) {
				verifyimageActive(imgElement);
			}
		}
		System.out.println("Total no. of invalid images are "	+ invalidImageCount);
	} catch (Exception e) {
		e.printStackTrace();
		System.out.println(e.getMessage());
	}
		
	}	
@AfterTest
public void aftertest() {
	if (driver != null)
	getDriver().quit();
}

		public void verifyimageActive(WebElement imgElement) {
		try 
		{
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(imgElement.getAttribute("src"));
			HttpResponse response = client.execute(request);
			if (response.getStatusLine().getStatusCode() != 200)
				invalidImageCount++;
		}
		catch (Exception e) {
		e.printStackTrace();
		}
		}   	 
}
