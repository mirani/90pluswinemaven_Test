package Runnable;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Master.Retry;
import Register.register_master;
public class Register_validation extends register_master {	
	
@BeforeTest
public void befortest() throws Exception,InterruptedException {
		deletescreenshot();
		driverset();
}	

@Test (priority = 1)
public void invalid_email() throws Exception, Throwable,InterruptedException {
	
			email = "test";
			row = 1;
			register_result_clear();
			final_rv_clear();
			Thread.sleep(2000);
			signup();
			Thread.sleep(2000);
			age_verify();
			Thread.sleep(3000);
			signup_link();
			useremailaddress();
			registerbutton();
			Thread.sleep(1000);
			try
			{		
				String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("invalid_email"))).getText();

				if(emailinvalid.equals("Email address is invalid"))
				{
					getLogger().info("Email Validation Pass");
					col = 2;
					row = 3;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Email Validation Fail");
					col = 2;
					row = 3;
					testresult = "Fail";
					validation_result();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Email Validation Fail due to Object not found");
				col = 2;
				row = 3;
				testresult = "Fail";
				validation_result();
			}	
}
@Test (priority = 2, retryAnalyzer = Retry.class)
public void invalid_mobile() throws Exception, Throwable,InterruptedException {
	
			mobile = "1234";
			usermobile();
			registerbutton();
			try
			{
				String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("invalid_mobile"))).getText();
	
				if(emailinvalid.equals("Mobile number must contain 10 digits."))
				{
					getLogger().info("Mobile Validation Pass");
					col = 2;
					row = 4;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Mobile Validation Fail");
					col = 2;
					row = 4;
					testresult = "Fail";
					validation_result();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Mobile Validation Fail due to Object not found");
				col = 2;
				row = 4;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 3, retryAnalyzer = Retry.class)
public void invalid_password() throws Exception, Throwable,InterruptedException {
	
			password = "1234";
			userpassword();
			registerbutton();
			try
			{
				String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("invalid_password"))).getText();
		
				if(emailinvalid.equals("Your password must be a minimum of 6 characters."))
				{
					getLogger().info("Password Validation Pass");
					col = 2;
					row = 5;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Password Validation Fail");
					col = 2;
					row = 5;
					testresult = "Fail";
					validation_result();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Password Validation Fail due to Object not found");
				col = 2;
				row = 5;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 4, retryAnalyzer = Retry.class)
public void terms_not_accept() throws Exception, Throwable,InterruptedException {
	
			fullname = "Tech Holding";
			email = "techholding@mailinator.com";
			mobile = "1234567890";
			password = "123456";
			registerclear();
			userfullname();
			useremailaddress();
			usermobile();
			userpassword();
			registerbutton();
			Thread.sleep(2000);
			try
			{
				String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("accept_terms"))).getText();
	
				if(emailinvalid.equals("Please accept terms & conditions to proceed."))
				{
					getLogger().info("Terms & Condition Validation Pass");
					col = 2;
					row = 6;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Terms & Condition Validation Fail");
					col = 2;
					row = 6;
					testresult = "Fail";
					validation_result();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Terms & Condition Validation Fail due to Object not found");
				col = 2;
				row = 6;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 5, retryAnalyzer = Retry.class, groups = { "Duplicate"})
public void duplicate_email() throws Exception, Throwable,InterruptedException {
	
			fullname = "Tech Holding";
			email = "techholding@mailinator.com";
			mobile = "9879393101";
			password = "123456";
			registerclear();
			userfullname();
			useremailaddress();
			usermobile();
			userpassword();
			terms_accept();
			registerbutton();
			Thread.sleep(2000);
	
			try
			{
				String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("email_in_use"))).getText();
	
				if(emailinvalid.equals("This email address is already in use."))
				{
					getLogger().info("Email already exist Validation Pass");
					col = 2;
					row = 7;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Email already exist Validation Fail");
					col = 2;
					row = 7;
					testresult = "Fail";
					validation_result();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Email already exist Validation Fail due to Object not found");
				col = 2;
				row = 7;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}	
}
@Test (priority = 6, retryAnalyzer = Retry.class, groups = { "Duplicate"})
public void duplicate_mobile() throws Exception, Throwable,InterruptedException {
	
			fullname = "Tech Holding";
			email = "techholding@mailinator.com";
			mobile = "1234567890";
			password = "123456";
			registerclear();
			userfullname();
			useremailaddress();
			usermobile();
			userpassword();
			registerbutton();
			Thread.sleep(2000);
			try
			{
				String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("mobile_in_use"))).getText();
	
				if(emailinvalid.equals("This number is already in use."))
				{
					getLogger().info("Mobile already exist Validation Pass");
					col = 2;
					row = 8;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Mobile already exist Validation Fail");
					col = 2;
					row = 8;
					testresult = "Fail";
					validation_result();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Mobile already exist Validation Fail due to Object not found");
				col = 2;
				row = 8;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 7, retryAnalyzer = Retry.class)
public void blank_register_data() throws Exception, Throwable,InterruptedException {
	
			registerclear();
			registerbutton();
			try
			{
				String blankfullname = getDriver().findElement(By.xpath(getObj().getProperty("blank_fullname"))).getText();
	
				if(blankfullname.equals("This field is required."))
				{
					getLogger().info("Require Full Name Validation Pass");
					col = 2;
					row = 9;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Require Full Name Validation Fail");
					col = 2;
					row = 9;
					testresult = "Fail";
					validation_result();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Require Full Name Validation Fail due to Object not found");
				col = 2;
				row = 9;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try
			{
				String blankemail = getDriver().findElement(By.xpath(getObj().getProperty("blank_email"))).getText();
	
				if(blankemail.equals("This field is required."))
				{
					getLogger().info("Require Email Validation Pass");
					col = 2;
					row = 10;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Require Email Validation Fail");
					col = 2;
					row = 10;
					testresult = "Fail";
					validation_result();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Require Email Validation Fail due to Object not found");
				col = 2;
				row = 10;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try
			{
				String blankmobile = getDriver().findElement(By.xpath(getObj().getProperty("blank_mobile"))).getText();
	
				if(blankmobile.equals("This field is required."))
				{
					getLogger().info("Require Mobile Validation Pass");
					col = 2;
					row = 11;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Require Mobile Validation Fail");
					col = 2;
					row = 11;
					testresult = "Fail";
					validation_result();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Require Mobile Validation Fail");
				col = 2;
				row = 11;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try
			{
				String blankpassword = getDriver().findElement(By.xpath(getObj().getProperty("blank_password"))).getText();
	
				if(blankpassword.equals("This field is required."))
				{
					getLogger().info("Require Password Validation Pass");
					col = 2;
					row = 12;
					testresult = "Pass";
					validation_result();
				}
				else
				{
					getLogger().info("Require Password Validation Fail");
					col = 2;
					row = 12;
					testresult = "Fail";
					validation_result();
				}
			}
			catch(Exception e)
			{
				getLogger().info("Require Password Validation Fail due to Object not found");
				col = 2;
				row = 12;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@AfterTest
public void aftertest() throws InterruptedException{
   getDriver().quit();
	}
	
}