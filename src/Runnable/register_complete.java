package Runnable;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import Master.DataCall;
import Register.register_master;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
public class register_complete extends DataCall {
	public class registercomplete extends register_master {
	
@BeforeTest
public void befortest() throws Exception {
		driverset();
}	

@Test
public void register() throws Exception, Throwable {
			dateemail = todaysdate + "@mailinator.com";
			datemobile = todaysdate;
			row = 1;
			final_rc_clear();
			setregisterdetails();
			registerData();
			QuizQuestion();
			shippingData();
			Thread.sleep(4000);
			signup();
			Thread.sleep(3000);
			age_verify();
			Thread.sleep(2000);
			signup_link();
			Thread.sleep(2000);
			userfullname();
			userdateemailaddress();
			userdatemobile();
			userpassword();
			terms_accept();
			registerbutton();			
			Thread.sleep(4000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("start_quiz_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("start_quiz_button"))).click();
				getLogger().info("User has just Start The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Start The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Quiz_not_Started";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(3000);
			try
			{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className("itemName")));
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Red1.contentEquals("1") && step1.getText().contentEquals("Red"))
				{
					step1.click();
					Red1 = "2";
				}
				if(White1.contentEquals("1") && step1.getText().contentEquals("White"))
				{
					step1.click();
					White1 = "2";
				}
				if(Rose1.contentEquals("1") && step1.getText().contentEquals("Rosé"))
				{
					step1.click();
					Rose1 = "2";
				}
				if(Sparkling1.contentEquals("1") && step1.getText().contentEquals("Sparkling"))
				{
					step1.click();
					Sparkling1 = "2";
				}	
				}
			}
			catch(Exception e)
			{
				TestReason = "Step 1 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Ans_not_complete";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 1 Completed of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step 1 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Button_Not_Click";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (RedMeats2.contentEquals("1") && step1.getText().contentEquals("Red Meats"))
				{
					step1.click();
					RedMeats2 = "2";
				}
				if(Poultry2.contentEquals("1") && step1.getText().contentEquals("Poultry"))
				{
					step1.click();
					Poultry2 = "2";
				}
				if(Seafood2.contentEquals("1") && step1.getText().contentEquals("Seafood"))
				{
					step1.click();
					Seafood2 = "2";
				}
				if(French2.contentEquals("1") && step1.getText().contentEquals("French"))
				{
					step1.click();
					French2 = "2";
				}
				if(Italian2.contentEquals("1") && step1.getText().contentEquals("Italian"))
				{
					step1.click();
					Italian2 = "2";
				}
				if(Mexican2.contentEquals("1") && step1.getText().contentEquals("Mexican"))
				{
					step1.click();
					Mexican2 = "2";
				}
				if(Japanese2.contentEquals("1") && step1.getText().contentEquals("Japanese"))
				{
					step1.click();
					Japanese2 = "2";
				}
				if(Chinese2.contentEquals("1") && step1.getText().contentEquals("Chinese"))
				{
					step1.click();
					Chinese2 = "2";
				}
				if(MiddleEastern2.contentEquals("1") && step1.getText().contentEquals("Middle Eastern"))
				{
					step1.click();
					MiddleEastern2 = "2";
				}
				if(Indian2.contentEquals("1") && step1.getText().contentEquals("Indian"))
				{
					step1.click();
					Indian2 = "2";
				}
				}
				getLogger().info("Step 2 answer Completed of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step 2 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_not_completed";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 2 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step 2 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Button_Not_Click";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Coffee3.contentEquals("1") && step1.getText().contentEquals("Coffee"))
				{
					step1.click();
					Coffee3 = "2";
				}
				if(Tea3.contentEquals("1") && step1.getText().contentEquals("Tea"))
				{
					step1.click();
					Tea3 = "2";
				}
				if(FruitJuices3.contentEquals("1") && step1.getText().contentEquals("Fruit Juices"))
				{
					step1.click();
					FruitJuices3 = "2";
				}
				if(VegetableJuices3.contentEquals("1") && step1.getText().contentEquals("Vegetable Juices"))
				{
					step1.click();
					VegetableJuices3 = "2";
				}
				if(Vodka3.contentEquals("1") && step1.getText().contentEquals("Vodka"))
				{
					step1.click();
					Vodka3 = "2";
				}
				if(Tequila3.contentEquals("1") && step1.getText().contentEquals("Tequila"))
				{
					step1.click();
					Tequila3 = "2";
				}
				if(Beer3.contentEquals("1") && step1.getText().contentEquals("Beer"))
				{
					step1.click();
					Beer3 = "2";
				}
				if(Whiskey3.contentEquals("1") && step1.getText().contentEquals("Whiskey"))
				{
					step1.click();
					Whiskey3 = "2";
				}
				if(Rum3.contentEquals("1") && step1.getText().contentEquals("Rum"))
				{
					step1.click();
					Rum3 = "2";
				}
				if(MixedDrink3.contentEquals("1") && step1.getText().contentEquals("Mixed Drink"))
				{
					step1.click();
					MixedDrink3 = "2";
				}
				}
				getLogger().info("Step 3 answer Completed of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step 3 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step3_quiz_answer_not_complete";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 3 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step 3 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Button_Not_Click";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className("itemName")));
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Dark4.contentEquals("1") && step1.getText().contentEquals("Dark"))
				{
					step1.click();
					Dark4 = "2";
				}
				if(Milk4.contentEquals("1") && step1.getText().contentEquals("Milk"))
				{
					step1.click();
					Milk4 = "2";
				}
				if(White4.contentEquals("1") && step1.getText().contentEquals("White"))
				{
					step1.click();
					White4 = "2";
				}	
				}
				getLogger().info("Step 4 Completed of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step 4 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step4_quiz_answer_not_complete";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 4 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step 4 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Button_Not_Click";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
// ----------------------------------Question Answer Step 5-----------------------------------			
			try
			{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className("itemName")));
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Apple5.contentEquals("1") && step1.getText().contentEquals("Apple"))
				{
					step1.click();
					Apple5 = "2";
				}
				if (Peach5.contentEquals("1") && step1.getText().contentEquals("Peach"))
				{
					step1.click();
					Peach5 = "2";
				}
				if(Orange5.contentEquals("1") && step1.getText().contentEquals("Orange"))
				{
					step1.click();
					Orange5 = "2";
				}
				if(Banana5.contentEquals("1") && step1.getText().contentEquals("Banana"))
				{
					step1.click();
					Banana5 = "2";
				}
				if(Grapes5.contentEquals("1") && step1.getText().contentEquals("Grapes"))
				{
					step1.click();
					Grapes5 = "2";
				}
				if(Berries5.contentEquals("1") && step1.getText().contentEquals("Berries"))
				{
					step1.click();
					Berries5 = "2";
				}
				}
				getLogger().info("Step 5 Completed of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step 5 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Ste5_quiz_answer_not_complete";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 5 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 5 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Button_Not_Click";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			// ----------------------------------Question Answer Step 6-----------------------------------			

			try
			{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className("itemName")));
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Rose6.contentEquals("1") && step1.getText().contentEquals("Rose"))
				{
					step1.click();
					Rose6 = "2";
				}
				if(WetForest6.contentEquals("1") && step1.getText().contentEquals("Wet Forest"))
				{
					step1.click();
					WetForest6 = "2";
				}
				if(Beach6.contentEquals("1") && step1.getText().contentEquals("Beach"))
				{
					step1.click();
					Beach6 = "2";
				}
				if(Leather6.contentEquals("1") && step1.getText().contentEquals("Leather"))
				{
					step1.click();
					Leather6 = "2";
				}
				if(Berries6.contentEquals("1") && step1.getText().contentEquals("Berries"))
				{
					step1.click();
					Berries6 = "2";
				}
				if(Coffee6.contentEquals("1") && step1.getText().contentEquals("Coffee"))
				{
					step1.click();
					Coffee6 = "2";
				}
				if(Citrus6.contentEquals("1") && step1.getText().contentEquals("Citrus"))
				{
					step1.click();
					Citrus6 = "2";
				}
				}
				getLogger().info("Step 6 Completed of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step 6 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_ans_not_complete";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 6 Completed of The Quiz");		
			}
			catch(Exception e)
			{
				TestReason = "Step 6 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Button_Not_Click";
				getscreenshot();
				driver.quit();
			}
			Thread.sleep(2000);

			// ----------------------------------Question Answer Step 7-----------------------------------			

			try
			{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className("itemName")));
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Spicy7.contentEquals("1") && step1.getText().contentEquals("Spicy"))
				{
					step1.click();
					Spicy7 = "2";
				}
				if(Salty7.contentEquals("1") && step1.getText().contentEquals("Salty/Savoury"))
				{
					step1.click();
					Salty7 = "2";
				}
				if(Sour7.contentEquals("1") && step1.getText().contentEquals("Sour"))
				{
					step1.click();
					Sour7 = "2";
				}
				if(Sweet7.contentEquals("1") && step1.getText().contentEquals("Sweet"))
				{
					step1.click();
					Sweet7 = "2";
				}
				if(GardenHerbs7.contentEquals("1") && step1.getText().contentEquals("Garden Herbs"))
				{
					step1.click();
					GardenHerbs7 = "2";
				}
				}
				getLogger().info("Step 7 Completed of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step 7 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_ans_not_complete";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 7 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 7 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Button_Not_Click";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			
			// ----------------------------------Question Answer Step 8-----------------------------------			
			
			try
			{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className("itemName")));
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Fruity8.contentEquals("1") && step1.getText().contentEquals("Fruity"))
				{
					step1.click();
					Fruity8 = "2";
				}
				if(Dry8.contentEquals("1") && step1.getText().contentEquals("Dry"))
				{
					step1.click();
					Dry8 = "2";
				}
				if(Spicy8.contentEquals("1") && step1.getText().contentEquals("Spicy"))
				{
					step1.click();
					Spicy8 = "2";
				}
				if(LightBody8.contentEquals("1") && step1.getText().contentEquals("Light Body"))
				{
					step1.click();
					LightBody8 = "2";
				}
				if(MediumBody8.contentEquals("1") && step1.getText().contentEquals("Medium Body"))
				{
					step1.click();
					MediumBody8 = "2";
				}
				if(FullBody8.contentEquals("1") && step1.getText().contentEquals("Full Body"))
				{
					step1.click();
					FullBody8 = "2";
				}
				}
				getLogger().info("Step 8 Completed of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step 8 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_ans_not_complete";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 8 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step 8 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Button_Not_Click";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			// ----------------------------------Question Answer Step 9-----------------------------------			
			
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("comment9q"))));
				getDriver().findElement(By.xpath(getObj().getProperty("comment9q"))).sendKeys(Comment9);
				getLogger().info("Step 9 Completed of The Quiz");			
			}
			catch(Exception e)
			{
				TestReason = "Step 9 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step9_quiz_ans_not_complete";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("finish_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("finish_quiz"))).click();
				getLogger().info("Step 9 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 9 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step9_Quiz_Button_Not_Click";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(3000);
//---------------------------------------------Quiz Confirmation-----------------------------------------------
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("comp_shi_pref"))));
				getDriver().findElement(By.xpath(getObj().getProperty("comp_shi_pref"))).click();
				TestReason = "Quiz Completed Successfully";
				getLogger().info(TestReason);
			}
			catch(Exception e)
			{
				TestReason = "Quiz not completed Successfully.";
				getLogger().info(TestReason);
				screenshotname = "User_Not_Redirect_Home";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{		
			List<WebElement> priceranges = getDriver().findElements(By.className("optName"));
			for(WebElement pricerange:priceranges)
			{
			    if(price_range.contentEquals("$12 - $15")  && pricerange.getText().contentEquals("$12 - $15")) 
			    {
			    	pricerange.click(); 
			        break;
			    }
			    if(price_range.contentEquals("$15 - $30")  && pricerange.getText().contentEquals("$15 - $30")) 
			    {
			    	pricerange.click(); 
			        break;
			    }
			    if(price_range.contentEquals("$30 - $50")  && pricerange.getText().contentEquals("$30 - $50")) 
			    {
			    	pricerange.click(); 
			        break;
			    }
			    if(price_range.contentEquals("$75 - $100")  && pricerange.getText().contentEquals("$75 - $100")) 
			    {
			    	pricerange.click(); 
			        break;
			    }
			}
			}
			catch(Exception e)
			{
				TestReason = "Price Range option not found";
				getLogger().info(TestReason);
				screenshotname = "Price_Range_Option_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{		
			List<WebElement> bottleships = getDriver().findElements(By.className("optName"));
			for(WebElement bottleship:bottleships)
			{								
			    if(bottle_ship.contentEquals("2")  && bottleship.getText().contentEquals("2 Bottles")) 
			    {
			    	bottleship.click(); 
			        break;
			    }
			    if(bottle_ship.contentEquals("3")  && bottleship.getText().contentEquals("3 Bottles")) 
			    {
			    	bottleship.click(); 
			        break;
			    }
			    if(bottle_ship.contentEquals("4")  && bottleship.getText().contentEquals("4 Bottles")) 
			    {
			    	bottleship.click(); 
			        break;
			    }
			    if(bottle_ship.contentEquals("6")  && bottleship.getText().contentEquals("6 Bottles")) 
			    {
			    	bottleship.click(); 
			        break;
			    }
			}
			}
			catch(Exception e)
			{
				TestReason = "No of Bottle option not found";
				getLogger().info(TestReason);
				screenshotname = "No_of_Bottle_Option_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{		
			List<WebElement> noofbottles = getDriver().findElements(By.className("optName"));
			for(WebElement noofbottle:noofbottles)
			{								
			    if(bottle_ship.contentEquals("1")  && noofbottle.getText().contentEquals("1 time a year")) 
			    {
			    	noofbottle.click(); 
			        break;
			    }
			    if(bottle_ship.contentEquals("2")  && noofbottle.getText().contentEquals("2 times a year")) 
			    {
			    	noofbottle.click(); 
			        break;
			    }
			    if(bottle_ship.contentEquals("3")  && noofbottle.getText().contentEquals("3 times a year")) 
			    {
			    	noofbottle.click(); 
			        break;
			    }
			    if(bottle_ship.contentEquals("4")  && noofbottle.getText().contentEquals("4 times a year")) 
			    {
			    	noofbottle.click(); 
			        break;
			    }
			}
			}
			catch(Exception e)
			{
				TestReason = "No of Times Bottle option not found";
				getLogger().info(TestReason);
				screenshotname = "No_of_Times_Bottle_Option_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("shipnext"))));
				getDriver().findElement(By.xpath(getObj().getProperty("shipnext"))).click();
				getLogger().info("Wine Preference Done");	
			}
			catch(Exception e)
			{
				TestReason = "Wine Preference has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Wine_Preference_Not_Done";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);	
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("add_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("add_address_button"))).click();
				getLogger().info("Address option open to enter");
			}
			catch(Exception e)
			{
				TestReason = "Address option not open.";
				getLogger().info(TestReason);
				screenshotname = "Add_Address_option_button_not_found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("address1"))));
				getDriver().findElement(By.xpath(getObj().getProperty("address1"))).sendKeys(streetaddress1);
				getLogger().info("Address1 Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Address1 field not found";
				getLogger().info(TestReason);
				screenshotname = "Address1_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("address2"))));
				getDriver().findElement(By.xpath(getObj().getProperty("address2"))).sendKeys(streetaddress2);
				getLogger().info("Address2 Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Address2 field not found";
				getLogger().info(TestReason);
				screenshotname = "Address2_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("city"))));
				getDriver().findElement(By.xpath(getObj().getProperty("city"))).sendKeys(city);
				getLogger().info("City Inserted");
			}
			catch(Exception e)
			{
				TestReason = "City field not found";
				getLogger().info(TestReason);
				screenshotname = "City_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("state"))));
				getDriver().findElement(By.xpath(getObj().getProperty("state"))).click();
			    WebElement dropdownValue = driver.findElement(By.xpath("//div[contains(text(),'"+state+"')]"));
			    dropdownValue.click();
				getLogger().info("State Selected");
			}
			catch(Exception e)
			{
				TestReason = "State field not found";
				getLogger().info(TestReason);
				screenshotname = "State_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("zip"))));
				getDriver().findElement(By.xpath(getObj().getProperty("zip"))).sendKeys(zip);
				getLogger().info("Zip Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Zip field not found";
				getLogger().info(TestReason);
				screenshotname = "Zip_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("save_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_address_button"))).click();
				getLogger().info("Address Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save address button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Address_button_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cardname"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cardname"))).sendKeys(cardname);
				getLogger().info("Cardname Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Cardname field not found";
				getLogger().info(TestReason);
				screenshotname = "Cardname_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cardnumber"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cardnumber"))).sendKeys(cardnumber);
				getLogger().info("Card Number Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Card Number field not found";
				getLogger().info(TestReason);
				screenshotname = "Cardnumber_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("expdate"))));
				getDriver().findElement(By.xpath(getObj().getProperty("expdate"))).sendKeys(expdate);
				getLogger().info("Exp Date Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Exp Date field not found";
				getLogger().info(TestReason);
				screenshotname = "ExpDate_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cvv"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cvv"))).sendKeys(cvv);
				getLogger().info("CVV Inserted");
			}
			catch(Exception e)
			{
				TestReason = "CVV field not found";
				getLogger().info(TestReason);
				screenshotname = "CVV_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(1000);
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("zipcode"))));
				getDriver().findElement(By.xpath(getObj().getProperty("zipcode"))).sendKeys(zipcode);
				getLogger().info("Zipcode Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Zipcode field not found";
				getLogger().info(TestReason);
				screenshotname = "Zipcode_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}			
			Thread.sleep(4000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("completeorder"))));
				getDriver().findElement(By.xpath(getObj().getProperty("completeorder"))).click();
				getLogger().info("Completed order Success");
			}
			catch(Exception e)
			{
				TestReason = "Complete order button not found";
				getLogger().info(TestReason);
				screenshotname = "Complete_Order_button_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("shippre_edit"))));
				getDriver().findElement(By.xpath(getObj().getProperty("shippre_edit"))).click();
				TestResult = "Pass";
				TestReason = "Shipment_Pref Completed Successfully";
				getLogger().info(TestReason);
								
			}
			catch(Exception e)
			{
				TestReason = "Shipment_Pref not completed Successfully.";
				getLogger().info(TestReason);
				screenshotname = "Shipment_Prefnot_not_found_Home";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
				
			}
			Thread.sleep(3000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.className(getObj().getProperty("my_account"))));
				getDriver().findElement(By.className(getObj().getProperty("my_account"))).click();
				getLogger().info("My Account Link Click");
			}
			catch(Exception e)
			{
				TestReason = "My Account Link not found";
				getLogger().info(TestReason);
				screenshotname = "My_Account_Link_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("logout"))));
				getDriver().findElement(By.xpath(getObj().getProperty("logout"))).click();
				Thread.sleep(2000);
				TestReason = "Register Successfully Done";
				getLogger().info(TestReason);
				TestResult = "Pass";
				setregisterresult();
				col1 = 3;
				row1 = 1;
				final_result = "Pass";
				final_result();
			}
			catch(Exception e)
			{
				TestReason = "Register not successfully done";
				getLogger().info(TestReason);
				screenshotname = "Register_Not_Success";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				col1 = 3;
				row1 = 1;
				final_result = "Fail";
				final_result();
				driver.quit();
			}
		}
@AfterTest
public void aftertest() {
   getDriver().close();
}
	}
}