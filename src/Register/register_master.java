package Register;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import Master.DataCall;

public class register_master extends DataCall{
	
	public void signup() throws Exception 
	{
		try
		{
			getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("sign_up"))));
			getDriver().findElement(By.xpath(getObj().getProperty("sign_up"))).click();
			getLogger().info("Sign_Up Link open successfully.");
		}
		catch(Exception e)
		{
			TestReason = "Sign_Up Link not opened.";
			getLogger().info(TestReason);
			screenshotname = "Sign_Up_Link_Not_Opened";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
			driver.quit();
			
		}
	}
	public void age_verify() throws Exception 
	{
	try
	{
		getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("age_verify"))));
		getDriver().findElement(By.xpath(getObj().getProperty("age_verify"))).click();
		getLogger().info("Age verification successfully done.");
	}
	catch(Exception e)
	{
		TestReason = "Age verification not done.";
		getLogger().info(TestReason);
		screenshotname = "Age_Verification_Not_Done";
		getscreenshot();
		TestResult = "Fail";
		setregisterresult();
		driver.quit();
		
	}
	}
	public void signup_link() throws Exception 
	{
	try
	{
		getWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath(getObj().getProperty("sign_up_link"))));
		getDriver().findElement(By.xpath(getObj().getProperty("sign_up_link"))).click();
		getLogger().info("Sign Up link clicked successfully.");
	}
	catch(Exception e)
	{
		TestReason = "Sign Up link not found.";
		getLogger().info(TestReason);
		screenshotname = "Sign_Up_Link_Not_Found";
		getscreenshot();
		TestResult = "Fail";
		setregisterresult();
		driver.quit();
		
	}
	}
	public void userfullname() throws Exception
	{
		try
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("full_name"))));
			getDriver().findElement(By.xpath(getObj().getProperty("full_name"))).sendKeys(fullname);
			getLogger().info("Full Name Inserted");
		}
		catch(Exception e)
		{
			TestReason = "Full Name field not found";
			getLogger().info(TestReason);
			screenshotname = "FullName_Field_Not_Found";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
			driver.quit();
			
		}
	}
	public void useremailaddress() throws Exception
	{
		try
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("email"))));
			getDriver().findElement(By.xpath(getObj().getProperty("email"))).sendKeys(email);
			getLogger().info("Email Inserted");
		}
		catch(Exception e)
		{
			TestReason = "Email field not found";
			getLogger().info(TestReason);
			screenshotname = "Email_Field_Not_Found";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
			driver.quit();
		}	
	}
	public void usermobile() throws Exception
	{
		try
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("mobile"))));
			getDriver().findElement(By.xpath(getObj().getProperty("mobile"))).sendKeys(mobile);
			getLogger().info("Mobile No Inserted");
		}
		catch(Exception e)
		{
			TestReason = "Mobile field not found";
			getLogger().info(TestReason);
			screenshotname = "Mobile_Field_Not_Found";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
			driver.quit();
		}
	}
	public void userdateemailaddress() throws Exception
	{
		try
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("email"))));
			getDriver().findElement(By.xpath(getObj().getProperty("email"))).sendKeys(dateemail);
			getLogger().info("Email Inserted");
		}
		catch(Exception e)
		{
			TestReason = "Email field not found";
			getLogger().info(TestReason);
			screenshotname = "Email_Field_Not_Found";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
			driver.quit();
		}	
	}
	public void userdatemobile() throws Exception
	{
		try
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("mobile"))));
			getDriver().findElement(By.xpath(getObj().getProperty("mobile"))).sendKeys(datemobile);
			getLogger().info("Mobile No Inserted");
		}
		catch(Exception e)
		{
			TestReason = "Mobile field not found";
			getLogger().info(TestReason);
			screenshotname = "Mobile_Field_Not_Found";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
			driver.quit();
		}
	}
	public void userpassword() throws Exception
	{
		try
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("password"))));
			getDriver().findElement(By.xpath(getObj().getProperty("password"))).sendKeys(password);
			getLogger().info("Password Inserted");
		}
		catch(Exception e)
		{
			TestReason = "Password field not found";
			getLogger().info(TestReason);
			screenshotname = "Password_Field_Not_Found";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
			driver.quit();
		}
	}
	public void terms_accept() throws Exception
	{
		try
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className(getObj().getProperty("term_accept"))));
			getDriver().findElement(By.className(getObj().getProperty("term_accept"))).click();
			getLogger().info("Agree Terms and Condition");
		}
		catch(Exception e)
		{
			TestReason = "Terms & Condition option not found";
			getLogger().info(TestReason);
			screenshotname = "Terms_Condition_Not_Found_TC_No_" + row;
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
			driver.quit();
			
		}
	}
	public void registerbutton() throws Exception
	{
		try
		{
			getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("register_button"))));
			getDriver().findElement(By.xpath(getObj().getProperty("register_button"))).click();
			getLogger().info("Sign Up Now button clicked.");
		}
		catch(Exception e)
		{
			TestReason = "Sign Up button not found";
			getLogger().info(TestReason);
			screenshotname = "Sign_Up_Button_Not_Found";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
			driver.quit();
		}	
	}
	public void registerclear() throws Exception
	{
		getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("full_name"))));
		getDriver().findElement(By.xpath(getObj().getProperty("full_name"))).clear();
		getDriver().findElement(By.xpath(getObj().getProperty("email"))).clear();
		getDriver().findElement(By.xpath(getObj().getProperty("mobile"))).clear();
		getDriver().findElement(By.xpath(getObj().getProperty("password"))).clear();
	}
	public void winepricerange() throws Exception
	{
		List<WebElement> priceranges = getDriver().findElements(By.className("optName"));
		for(WebElement pricerange:priceranges)
		{
		    if(price_range.contentEquals("$12 - $15")  && pricerange.getText().contentEquals("$12 - $15")) 
		    {
		    	pricerange.click(); 
		        break;
		    }
		    if(price_range.contentEquals("$15 - $30")  && pricerange.getText().contentEquals("$15 - $30")) 
		    {
		    	pricerange.click(); 
		        break;
		    }
		    if(price_range.contentEquals("$30 - $50")  && pricerange.getText().contentEquals("$30 - $50")) 
		    {
		    	pricerange.click(); 
		        break;
		    }
		    if(price_range.contentEquals("$75 - $100")  && pricerange.getText().contentEquals("$75 - $100")) 
		    {
		    	pricerange.click(); 
		        break;
		    }
		}
	}
	public void noofbottleship() throws Exception
	{
		List<WebElement> bottleships = getDriver().findElements(By.className("optName"));
		for(WebElement bottleship:bottleships)
		{								
		    if(bottle_ship.contentEquals("2")  && bottleship.getText().contentEquals("2 Bottles")) 
		    {
		    	bottleship.click(); 
		        break;
		    }
		    if(bottle_ship.contentEquals("3")  && bottleship.getText().contentEquals("3 Bottles")) 
		    {
		    	bottleship.click(); 
		        break;
		    }
		    if(bottle_ship.contentEquals("4")  && bottleship.getText().contentEquals("4 Bottles")) 
		    {
		    	bottleship.click(); 
		        break;
		    }
		    if(bottle_ship.contentEquals("6")  && bottleship.getText().contentEquals("6 Bottles")) 
		    {
		    	bottleship.click(); 
		        break;
		    }
		}
	}
	public void nooftimeship() throws Exception
	{
		List<WebElement> noofbottles = getDriver().findElements(By.className("optName"));
		for(WebElement noofbottle:noofbottles)
		{								
		    if(bottle_ship.contentEquals("1")  && noofbottle.getText().contentEquals("1 time a year")) 
		    {
		    	noofbottle.click(); 
		        break;
		    }
		    if(bottle_ship.contentEquals("2")  && noofbottle.getText().contentEquals("2 times a year")) 
		    {
		    	noofbottle.click(); 
		        break;
		    }
		    if(bottle_ship.contentEquals("3")  && noofbottle.getText().contentEquals("3 times a year")) 
		    {
		    	noofbottle.click(); 
		        break;
		    }
		    if(bottle_ship.contentEquals("4")  && noofbottle.getText().contentEquals("4 times a year")) 
		    {
		    	noofbottle.click(); 
		        break;
		    }
		}
	}
	
}
