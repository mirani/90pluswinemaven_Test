package Register;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import Master.DataCall;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
public class register_success extends DataCall {
	
@BeforeTest
public void befortest() throws Exception {
		driverset();
}	

@Test
public void register() throws Exception, Throwable {
			totalNoOfRows = sh.getRows();
			for (row = 1; row < totalNoOfRows; row++) {
			fullname = sh.getCell(0,row).getContents();
			email = sh.getCell(1,row).getContents();
			mobile = sh.getCell(2,row).getContents();
			password = sh.getCell(3,row).getContents();
			
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("sign_up"))));
				getDriver().findElement(By.xpath(getObj().getProperty("sign_up"))).click();
				getLogger().info("Sign_Up Link open successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Sign_Up Link not opened.";
				getLogger().info(TestReason);
				screenshotname = "Sign_Up_Link_Not_Opened_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			if(row==1)
			{	
				try
				{
					getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("age_verify"))));
					getDriver().findElement(By.xpath(getObj().getProperty("age_verify"))).click();
					getLogger().info("Age verification successfully done.");
				}
				catch(Exception e)
				{
					TestReason = "Age verification not done.";
					getLogger().info(TestReason);
					screenshotname = "Age_Verification_Not_Done_TC_No_" + row;
					getscreenshot();
					TestResult = "Fail";
					setregisterresult();
					break;
				}
			}
			Thread.sleep(3000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("sign_up_link"))));
				getDriver().findElement(By.xpath(getObj().getProperty("sign_up_link"))).click();
				getLogger().info("Sign Up link clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Sign Up link not found.";
				getLogger().info(TestReason);
				screenshotname = "Sign_Up_Link_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}

			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("full_name"))));
				getDriver().findElement(By.xpath(getObj().getProperty("full_name"))).sendKeys(fullname);
				getLogger().info("Full Name Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Full Name field not found";
				getLogger().info(TestReason);
				screenshotname = "FullName_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("email"))));
				getDriver().findElement(By.xpath(getObj().getProperty("email"))).sendKeys(email);
				getLogger().info("Email Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Email field not found";
				getLogger().info(TestReason);
				screenshotname = "Email_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("mobile"))));
				getDriver().findElement(By.xpath(getObj().getProperty("mobile"))).sendKeys(mobile);
				getLogger().info("Mobile No Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Mobile field not found";
				getLogger().info(TestReason);
				screenshotname = "Mobile_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("password"))));
				getDriver().findElement(By.xpath(getObj().getProperty("password"))).sendKeys(password);
				getLogger().info("Password Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Password_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className(getObj().getProperty("term_accept"))));
				getDriver().findElement(By.className(getObj().getProperty("term_accept"))).click();
				getLogger().info("Agree Terms and Condition");
			}
			catch(Exception e)
			{
				TestReason = "Terms & Condition option not found";
				getLogger().info(TestReason);
				screenshotname = "Terms_Condition_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("register_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("register_button"))).click();
				getLogger().info("Sign Up Now button clicked.");
			}
			catch(Exception e)
			{
				TestReason = "Sign Up button not found";
				getLogger().info(TestReason);
				screenshotname = "Sign_Up_Button_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("start_quiz_button"))));
				driver.navigate().to(getObj().getProperty("URL"));
				getLogger().info("User redirect to Home Page");
				
			}
			catch(Exception e)
			{
				TestReason = "User not redirect successfully to Home Page.";
				getLogger().info(TestReason);
				screenshotname = "User_Not_Redirect_Home_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			Thread.sleep(3000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.className(getObj().getProperty("my_account"))));
				getDriver().findElement(By.className(getObj().getProperty("my_account"))).click();
				getLogger().info("My Account Link Click");
			}
			catch(Exception e)
			{
				TestReason = "My Account Link not found";
				getLogger().info(TestReason);
				screenshotname = "My_Account_Link_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("logout"))));
				getDriver().findElement(By.xpath(getObj().getProperty("logout"))).click();
				Thread.sleep(2000);
				getDriver().findElement(By.xpath(getObj().getProperty("close_menu"))).click();
				TestResult = "Pass";
				TestReason = "Register Successfully Done";
				getLogger().info(TestReason);
				setregisterresult();
			}
			catch(Exception e)
			{
				TestReason = "Register not successfully done";
				getLogger().info(TestReason);
				screenshotname = "Register_Not_Success_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
		}
}
@AfterTest
public void aftertest() {
   getDriver().close();
}
}