package Register;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import Master.DataCall;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
public class register_dummy_user extends DataCall {
	
@BeforeTest
public void befortest() throws Exception {
		driverset();
}	

@Test
public void register() throws Exception, Throwable {
			fullname = "Tech Holding";
			email = "techholding@gustr.com";
			mobile = "1234567899";
			password = "123456";
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("sign_up"))));
				getDriver().findElement(By.xpath(getObj().getProperty("sign_up"))).click();
				getLogger().info("Sign_Up Link open successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Sign_Up Link not opened.";
				getLogger().info(TestReason);
				screenshotname = "Sign_Up_Link_Not_Opened";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("age_verify"))));
				getDriver().findElement(By.xpath(getObj().getProperty("age_verify"))).click();
				getLogger().info("Age verification successfully done.");
			}
			catch(Exception e)
			{
				TestReason = "Age verification not done.";
				getLogger().info(TestReason);
				screenshotname = "Age_Verification_Not_Done";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
			}
			Thread.sleep(3000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("sign_up_link"))));
				getDriver().findElement(By.xpath(getObj().getProperty("sign_up_link"))).click();
				getLogger().info("Sign Up link clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Sign Up link not found.";
				getLogger().info(TestReason);
				screenshotname = "Sign_Up_Link_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				
			}

			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("full_name"))));
				getDriver().findElement(By.xpath(getObj().getProperty("full_name"))).sendKeys(fullname);
				getLogger().info("Full Name Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Full Name field not found";
				getLogger().info(TestReason);
				screenshotname = "FullName_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("email"))));
				getDriver().findElement(By.xpath(getObj().getProperty("email"))).sendKeys(email);
				getLogger().info("Email Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Email field not found";
				getLogger().info(TestReason);
				screenshotname = "Email_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				
			}
			
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("mobile"))));
				getDriver().findElement(By.xpath(getObj().getProperty("mobile"))).sendKeys(mobile);
				getLogger().info("Mobile No Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Mobile field not found";
				getLogger().info(TestReason);
				screenshotname = "Mobile_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				
			}
			
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("password"))));
				getDriver().findElement(By.xpath(getObj().getProperty("password"))).sendKeys(password);
				getLogger().info("Password Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Password_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				
			}
			
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className(getObj().getProperty("term_accept"))));
				getDriver().findElement(By.className(getObj().getProperty("term_accept"))).click();
				getLogger().info("Agree Terms and Condition");
			}
			catch(Exception e)
			{
				TestReason = "Terms & Condition option not found";
				getLogger().info(TestReason);
				screenshotname = "Terms_Condition_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				
			}
			
			
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("register_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("register_button"))).click();
				getLogger().info("Sign Up Now button clicked.");
			}
			catch(Exception e)
			{
				TestReason = "Sign Up button not found";
				getLogger().info(TestReason);
				screenshotname = "Sign_Up_Button_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				
			}
			
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("start_quiz_button"))));
				driver.navigate().to(getObj().getProperty("URL"));
				getLogger().info("User redirect to Home Page");
				
			}
			catch(Exception e)
			{
				TestReason = "User not redirect successfully to Home Page.";
				getLogger().info(TestReason);
				screenshotname = "User_Not_Redirect_Home";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				
			}
			Thread.sleep(3000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.className(getObj().getProperty("my_account"))));
				getDriver().findElement(By.className(getObj().getProperty("my_account"))).click();
				getLogger().info("My Account Link Click");
			}
			catch(Exception e)
			{
				TestReason = "My Account Link not found";
				getLogger().info(TestReason);
				screenshotname = "My_Account_Link_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				
			}
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("logout"))));
				getDriver().findElement(By.xpath(getObj().getProperty("logout"))).click();
				Thread.sleep(2000);
				getDriver().findElement(By.xpath(getObj().getProperty("close_menu"))).click();
				TestResult = "Pass";
				TestReason = "Register Successfully Done";
				getLogger().info(TestReason);
				setregisterresult();
			}
			catch(Exception e)
			{
				TestReason = "Register not successfully done";
				getLogger().info(TestReason);
				screenshotname = "Register_Not_Success";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				
			}
		}
@AfterTest
public void aftertest() {
   getDriver().close();
}
}