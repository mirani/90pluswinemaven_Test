package Register;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import Master.DataCall;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
public class register_validation extends DataCall {
	public class validation extends register_master {

	
@BeforeTest
public void befortest() throws Exception {
		driverset();
}	

@Test (priority = 1)
public void invalid_email() throws Exception, Throwable {
	
	email = "test";
	signup();
	age_verify();
	signup_link();
	useremailaddress();
	registerbutton();
	
	String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("invalid_email"))).getText();
	
	if(emailinvalid.equals("Email address is invalid"))
	{
		System.out.print("Email Validation Pass");
	}
	else
	{
		System.out.print("Email Validation Fail");
	}
}
@Test (priority = 2)
public void invalid_mobile() throws Exception, Throwable {
	
	mobile = "1234";
	usermobile();
	registerbutton();
	
	String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("invalid_mobile"))).getText();
	
	if(emailinvalid.equals("Mobile number must contain 10 digits."))
	{
		System.out.print("Mobile Validation Pass");
	}
	else
	{
		System.out.print("Mobile Validation Fail");
	}
}
@Test (priority = 3)
public void invalid_password() throws Exception, Throwable {
	
	password = "1234";
	userpassword();
	registerbutton();
	
	String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("invalid_password"))).getText();
	
	if(emailinvalid.equals("Your password must be a minimum of 6 characters."))
	{
		System.out.print("Password Validation Pass");
	}
	else
	{
		System.out.print("Password Validation Fail");
	}
}
@Test (priority = 4)
public void terms_not_accept() throws Exception, Throwable {
	
	fullname = "Tech Holding";
	email = "techholding@gustr.com";
	mobile = "1234567890";
	password = "123456";
	registerclear();
	userfullname();
	useremailaddress();
	usermobile();
	userpassword();
	registerbutton();
	
	String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("accept_terms"))).getText();
	
	if(emailinvalid.equals("Please accept terms & conditions to proceed."))
	{
		System.out.print("Terms & Condition Validation Pass");
	}
	else
	{
		System.out.print("Terms & Condition Validation Fail");
	}
}
@Test (priority = 5)
public void duplicate_email() throws Exception, Throwable {
	
	fullname = "Tech Holding";
	email = "techholding@gustr.com";
	mobile = "1234567899";
	password = "123456";
	registerclear();
	userfullname();
	useremailaddress();
	usermobile();
	userpassword();
	terms_accept();
	registerbutton();
	Thread.sleep(2000);
	
	String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("email_in_use"))).getText();
	
	if(emailinvalid.equals("This email address is already in use."))
	{
		System.out.print("Email already exist Validation Pass");
	}
	else
	{
		System.out.print("Email already exist Validation Fail");
	}
}
@Test (priority = 6)
public void duplicate_mobile() throws Exception, Throwable {
	
	fullname = "Tech Holding";
	email = "techholding@gustr.com";
	mobile = "1234567890";
	password = "123456";
	registerclear();
	userfullname();
	useremailaddress();
	usermobile();
	userpassword();
	registerbutton();
	Thread.sleep(2000);
	
	String emailinvalid = getDriver().findElement(By.xpath(getObj().getProperty("mobile_in_use"))).getText();
	
	if(emailinvalid.equals("This number is already in use."))
	{
		System.out.print("Mobile already exist Validation Pass");
	}
	else
	{
		System.out.print("Mobile already exist Validation Fail");
	}
}
@AfterTest
public void aftertest() {
   getDriver().close();
}
	}
}