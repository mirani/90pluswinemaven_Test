package Quiz;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import Master.DataCall;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
public class StartTheQuiz extends DataCall {
	
@BeforeTest
public void befortest() throws Exception {
		driverset();
}	

@Test
public void StartQuiz() throws Exception, Throwable {
			totalNoOfRows = sh1.getRows();
			for (row = 1; row < totalNoOfRows; row++) {
			
			QuizQuestion();
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login"))).click();
				getLogger().info("Login Link open successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Login Link not opened.";
				getLogger().info(TestReason);
				screenshotname = "Login_Link_Not_Opened_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login_link"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login_link"))).click();
				getLogger().info("Login link clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Login link not found.";
				getLogger().info(TestReason);
				screenshotname = "Login_Link_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("email"))));
				getDriver().findElement(By.xpath(getObj().getProperty("email"))).sendKeys(email);
				getLogger().info("Email Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Email field not found";
				getLogger().info(TestReason);
				screenshotname = "Email_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("password"))));
				getDriver().findElement(By.xpath(getObj().getProperty("password"))).sendKeys(password);
				getLogger().info("Password Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Password_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login_button"))).click();
				getLogger().info("Login Now button clicked.");
			}
			catch(Exception e)
			{
				TestReason = "Login Now button not found";
				getLogger().info(TestReason);
				screenshotname = "Loginn_Now_Button_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("start_quiz_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("start_quiz_button"))).click();
				getLogger().info("User has just Start The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Start The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Quiz_not_Started_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			Thread.sleep(3000);
			try
			{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className("itemName")));
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Red1.contentEquals("1") && step1.getText().contentEquals("Red"))
				{
					step1.click();
					Red1 = "2";
				}
				if(White1.contentEquals("1") && step1.getText().contentEquals("White"))
				{
					step1.click();
					White1 = "2";
				}
				if(Rose1.contentEquals("1") && step1.getText().contentEquals("Rosé"))
				{
					step1.click();
					Rose1 = "2";
				}
				if(Sparkling1.contentEquals("1") && step1.getText().contentEquals("Sparkling"))
				{
					step1.click();
					Sparkling1 = "2";
				}	
				}
			}
			catch(Exception e)
			{
				TestReason = "Step 1 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Ans_not_complete_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 1 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 1 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (RedMeats2.contentEquals("1") && step1.getText().contentEquals("Red Meats"))
				{
					step1.click();
					RedMeats2 = "2";
				}
				if(Poultry2.contentEquals("1") && step1.getText().contentEquals("Poultry"))
				{
					step1.click();
					Poultry2 = "2";
				}
				if(Seafood2.contentEquals("1") && step1.getText().contentEquals("Seafood"))
				{
					step1.click();
					Seafood2 = "2";
				}
				if(French2.contentEquals("1") && step1.getText().contentEquals("French"))
				{
					step1.click();
					French2 = "2";
				}
				if(Italian2.contentEquals("1") && step1.getText().contentEquals("Italian"))
				{
					step1.click();
					Italian2 = "2";
				}
				if(Mexican2.contentEquals("1") && step1.getText().contentEquals("Mexican"))
				{
					step1.click();
					Mexican2 = "2";
				}
				if(Japanese2.contentEquals("1") && step1.getText().contentEquals("Japanese"))
				{
					step1.click();
					Japanese2 = "2";
				}
				if(Chinese2.contentEquals("1") && step1.getText().contentEquals("Chinese"))
				{
					step1.click();
					Chinese2 = "2";
				}
				if(MiddleEastern2.contentEquals("1") && step1.getText().contentEquals("Middle Eastern"))
				{
					step1.click();
					MiddleEastern2 = "2";
				}
				if(Indian2.contentEquals("1") && step1.getText().contentEquals("Indian"))
				{
					step1.click();
					Indian2 = "2";
				}
				}
				getLogger().info("Step 2 answer Completed of The Quiz");

			}
			catch(Exception e)
			{
				TestReason = "Step 2 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_not_completed_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 2 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 2 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Coffee3.contentEquals("1") && step1.getText().contentEquals("Coffee"))
				{
					step1.click();
					Coffee3 = "2";
				}
				if(Tea3.contentEquals("1") && step1.getText().contentEquals("Tea"))
				{
					step1.click();
					Tea3 = "2";
				}
				if(FruitJuices3.contentEquals("1") && step1.getText().contentEquals("Fruit Juices"))
				{
					step1.click();
					FruitJuices3 = "2";
				}
				if(VegetableJuices3.contentEquals("1") && step1.getText().contentEquals("Vegetable Juices"))
				{
					step1.click();
					VegetableJuices3 = "2";
				}
				if(Vodka3.contentEquals("1") && step1.getText().contentEquals("Vodka"))
				{
					step1.click();
					Vodka3 = "2";
				}
				if(Tequila3.contentEquals("1") && step1.getText().contentEquals("Tequila"))
				{
					step1.click();
					Tequila3 = "2";
				}
				if(Beer3.contentEquals("1") && step1.getText().contentEquals("Beer"))
				{
					step1.click();
					Beer3 = "2";
				}
				if(Whiskey3.contentEquals("1") && step1.getText().contentEquals("Whiskey"))
				{
					step1.click();
					Whiskey3 = "2";
				}
				if(Rum3.contentEquals("1") && step1.getText().contentEquals("Rum"))
				{
					step1.click();
					Rum3 = "2";
				}
				if(MixedDrink3.contentEquals("1") && step1.getText().contentEquals("Mixed Drink"))
				{
					step1.click();
					MixedDrink3 = "2";
				}
				}
				getLogger().info("Step 3 answer Completed of The Quiz");

			}
			catch(Exception e)
			{
				TestReason = "Step 3 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step3_quiz_answer_not_complete_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 3 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 3 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className("itemName")));
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Dark4.contentEquals("1") && step1.getText().contentEquals("Dark"))
				{
					step1.click();
					Dark4 = "2";
				}
				if(Milk4.contentEquals("1") && step1.getText().contentEquals("Milk"))
				{
					step1.click();
					Milk4 = "2";
				}
				if(White4.contentEquals("1") && step1.getText().contentEquals("White"))
				{
					step1.click();
					White4 = "2";
				}	
				}
				getLogger().info("Step 4 Completed of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step 4 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step4_quiz_answer_not_complete_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 4 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 4 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
// ----------------------------------Question Answer Step 5-----------------------------------			
			try
			{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className("itemName")));
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Apple5.contentEquals("1") && step1.getText().contentEquals("Apple"))
				{
					step1.click();
					Apple5 = "2";
				}
				if (Peach5.contentEquals("1") && step1.getText().contentEquals("Peach"))
				{
					step1.click();
					Peach5 = "2";
				}
				if(Orange5.contentEquals("1") && step1.getText().contentEquals("Orange"))
				{
					step1.click();
					Orange5 = "2";
				}
				if(Banana5.contentEquals("1") && step1.getText().contentEquals("Banana"))
				{
					step1.click();
					Banana5 = "2";
				}
				if(Grapes5.contentEquals("1") && step1.getText().contentEquals("Grapes"))
				{
					step1.click();
					Grapes5 = "2";
				}
				if(Berries5.contentEquals("1") && step1.getText().contentEquals("Berries"))
				{
					step1.click();
					Berries5 = "2";
				}
				}
				getLogger().info("Step 5 Completed of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step 5 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Ste5_quiz_answer_not_complete_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 5 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 5 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			// ----------------------------------Question Answer Step 6-----------------------------------			

			try
			{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className("itemName")));
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Rose6.contentEquals("1") && step1.getText().contentEquals("Rose"))
				{
					step1.click();
					Rose6 = "2";
				}
				if(WetForest6.contentEquals("1") && step1.getText().contentEquals("Wet Forest"))
				{
					step1.click();
					WetForest6 = "2";
				}
				if(Beach6.contentEquals("1") && step1.getText().contentEquals("Beach"))
				{
					step1.click();
					Beach6 = "2";
				}
				if(Leather6.contentEquals("1") && step1.getText().contentEquals("Leather"))
				{
					step1.click();
					Leather6 = "2";
				}
				if(Berries6.contentEquals("1") && step1.getText().contentEquals("Berries"))
				{
					step1.click();
					Berries6 = "2";
				}
				if(Coffee6.contentEquals("1") && step1.getText().contentEquals("Coffee"))
				{
					step1.click();
					Coffee6 = "2";
				}
				if(Citrus6.contentEquals("1") && step1.getText().contentEquals("Citrus"))
				{
					step1.click();
					Citrus6 = "2";
				}
				}
				getLogger().info("Step 6 Completed of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step 6 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_ans_not_complete_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 6 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 6 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}

			// ----------------------------------Question Answer Step 7-----------------------------------			

			try
			{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className("itemName")));
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Spicy7.contentEquals("1") && step1.getText().contentEquals("Spicy"))
				{
					step1.click();
					Spicy7 = "2";
				}
				if(Salty7.contentEquals("1") && step1.getText().contentEquals("Salty/Savoury"))
				{
					step1.click();
					Salty7 = "2";
				}
				if(Sour7.contentEquals("1") && step1.getText().contentEquals("Sour"))
				{
					step1.click();
					Sour7 = "2";
				}
				if(Sweet7.contentEquals("1") && step1.getText().contentEquals("Sweet"))
				{
					step1.click();
					Sweet7 = "2";
				}
				if(GardenHerbs7.contentEquals("1") && step1.getText().contentEquals("Garden Herbs"))
				{
					step1.click();
					GardenHerbs7 = "2";
				}
				}
				getLogger().info("Step 7 Completed of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step 7 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_ans_not_complete_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 7 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 7 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			// ----------------------------------Question Answer Step 8-----------------------------------			
			
			try
			{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className("itemName")));
			List<WebElement> step1s = getDriver().findElements(By.className("itemName"));
			for(WebElement step1 : step1s)
			{				
				if (Fruity8.contentEquals("1") && step1.getText().contentEquals("Fruity"))
				{
					step1.click();
					Fruity8 = "2";
				}
				if(Dry8.contentEquals("1") && step1.getText().contentEquals("Dry"))
				{
					step1.click();
					Dry8 = "2";
				}
				if(Spicy8.contentEquals("1") && step1.getText().contentEquals("Spicy"))
				{
					step1.click();
					Spicy8 = "2";
				}
				if(LightBody8.contentEquals("1") && step1.getText().contentEquals("Light Body"))
				{
					step1.click();
					LightBody8 = "2";
				}
				if(MediumBody8.contentEquals("1") && step1.getText().contentEquals("Medium Body"))
				{
					step1.click();
					MediumBody8 = "2";
				}
				if(FullBody8.contentEquals("1") && step1.getText().contentEquals("Full Body"))
				{
					step1.click();
					FullBody8 = "2";
				}
				}
				getLogger().info("Step 8 Completed of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step 8 answer of The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_ans_not_complete_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 8 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 8 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			// ----------------------------------Question Answer Step 9-----------------------------------			
			
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("comment9q"))));
				getDriver().findElement(By.xpath(getObj().getProperty("comment9q"))).sendKeys(Comment9);
				getLogger().info("Step 9 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 9 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step9_quiz_ans_not_complete_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("finish_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("finish_quiz"))).click();
				getLogger().info("Step 9 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 9 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step9_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			Thread.sleep(3000);
//---------------------------------------------Quiz Confirmation-----------------------------------------------
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("quizcomplete"))));
				getDriver().findElement(By.xpath(getObj().getProperty("shipmentpref"))).click();
				TestResult = "Pass";
				TestReason = "Quiz Completed Successfully";
				getLogger().info(TestReason);
				setregisterresult();				
			}
			catch(Exception e)
			{
				TestReason = "Quiz not completed Successfully.";
				getLogger().info(TestReason);
				screenshotname = "User_Not_Redirect_Home_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
					
			
		}
}
@AfterTest
public void aftertest() {
   getDriver().close();
}
}