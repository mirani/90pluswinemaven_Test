package Quiz;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import Master.DataCall;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
public class StartTheQuiz_validation extends DataCall {
	
@BeforeTest
public void befortest() throws Exception {
		driverset();
}	

@Test
public void StartQuiz() throws Exception, Throwable {
			totalNoOfRows = sh1.getRows();
			for (row = 1; row < totalNoOfRows; row++) {
			
			QuizQuestion();
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login"))).click();
				getLogger().info("Login Link open successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Login Link not opened.";
				getLogger().info(TestReason);
				screenshotname = "Login_Link_Not_Opened_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login_link"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login_link"))).click();
				getLogger().info("Login link clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Login link not found.";
				getLogger().info(TestReason);
				screenshotname = "Login_Link_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("email"))));
				getDriver().findElement(By.xpath(getObj().getProperty("email"))).sendKeys(email);
				getLogger().info("Email Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Email field not found";
				getLogger().info(TestReason);
				screenshotname = "Email_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("password"))));
				getDriver().findElement(By.xpath(getObj().getProperty("password"))).sendKeys(password);
				getLogger().info("Password Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Password_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login_button"))).click();
				getLogger().info("Login Now button clicked.");
			}
			catch(Exception e)
			{
				TestReason = "Login Now button not found";
				getLogger().info(TestReason);
				screenshotname = "Loginn_Now_Button_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
//----------------------Start The Quiz Main------------------------------			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Start The Quiz Skip link pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Start The Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Start_Quiz_link_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Start Quiz Continue button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Start Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Start_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("start_quiz_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("start_quiz_button"))).click();
				getLogger().info("User has just Start The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Start The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Quiz_not_Started_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			Thread.sleep(3000);
			
//----------------------------Step 1 Quiz---------------------------------------			
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step1 Quiz Skip link pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step1 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_link_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step1 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step1 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step1 Quiz Exit button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Start The Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Exit_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step1 Quiz Continue button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step1 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 1 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 1 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step1 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step1 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 1 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 1 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step 1 Skip of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 1 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Skip_Not_Done";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			System.out.println("Step1 Completed successfully");
//-------------------------Step 2 Quiz--------------------------------
						
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step2 Quiz Skip link pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step2 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_link_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step2 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step2 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step2 Quiz Exit button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step2 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Exit_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step2 Quiz Continue button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step2 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 2 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 2 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step2 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step2 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 2 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 2 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step 2 Skip of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 2 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Skip_Not_Done";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			System.out.println("Step2 Completed successfully");

//-------------------------Step 3 Quiz--------------------------------
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step3 Quiz Skip link pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step3 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_link_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step3 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step3 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step3 Quiz Exit button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step3 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Exit_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step3 Quiz Continue button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step3 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 3 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 3 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step3 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step3 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 3 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 3 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step 3 Skip of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 3 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Skip_Not_Done";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			System.out.println("Step3 Completed successfully");

//-------------------------Step 4 Quiz--------------------------------
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step4 Quiz Skip link pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step4 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_link_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step4 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step4 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step4 Quiz Exit button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step4 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Exit_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step4 Quiz Continue button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step4 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 4 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 4 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step4 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step4 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 4 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 4 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step 4 Skip of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 4 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Skip_Not_Done";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			System.out.println("Step4 Completed successfully");

			
//-------------------------Step 5 Quiz--------------------------------
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step5 Quiz Skip link pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step5 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_link_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step5 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step5 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step5 Quiz Exit button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step5 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Exit_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step5 Quiz Continue button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step5 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 5 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 5 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step5 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step5 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 5 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 5 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step 5 Skip of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 5 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Skip_Not_Done";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			System.out.println("Step5 Completed successfully");

			
//-------------------------Step 6 Quiz--------------------------------
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step6 Quiz Skip link pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step6 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_link_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step6 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step6 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step6 Quiz Exit button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step6 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Exit_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step6 Quiz Continue button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step6 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 6 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 6 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step6 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step6 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 6 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 6 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step 6 Skip of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 6 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Skip_Not_Done";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			System.out.println("Step6 Completed successfully");

			
//-------------------------Step 7 Quiz--------------------------------
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step7 Quiz Skip link pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step7 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_link_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step7 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step7 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step7 Quiz Exit button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step7 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Exit_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step7 Quiz Continue button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step7 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 7 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 7 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step7 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step7 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 7 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 7 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step 7 Skip of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 7 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Skip_Not_Done";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			System.out.println("Step7 Completed successfully");
			
//-------------------------Step 8 Quiz--------------------------------
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step8 Quiz Skip link pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step8 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_link_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step8 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step8 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step8 Quiz Exit button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step8 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Exit_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step8 Quiz Continue button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step8 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 8 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 8 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step8 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step8 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step 8 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 8 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step 8 Skip of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 8 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Skip_Not_Done";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			System.out.println("Step8 Completed successfully");
			
//-------------------------Step 9 Quiz--------------------------------
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step9 Quiz Skip link pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step9 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step9_Quiz_link_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step9 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step9 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step9_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step9 Quiz Exit button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step9 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step9_Quiz_Exit_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step9 Quiz Continue button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step9 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step9_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("finish_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("finish_quiz"))).click();
				getLogger().info("Step 9 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step 9 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step9_Quiz_Button_Not_Click_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			System.out.println("Step9 Completed successfully");
			Thread.sleep(3000);
			
//---------------------------------------------Quiz Confirmation-----------------------------------------------
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("quizcomplete"))));
				getDriver().findElement(By.xpath(getObj().getProperty("shipmentpref"))).click();
				TestResult = "Pass";
				TestReason = "Quiz Completed Successfully";
				getLogger().info(TestReason);
				setregisterresult();				
			}
			catch(Exception e)
			{
				TestReason = "Quiz not completed Successfully.";
				getLogger().info(TestReason);
				screenshotname = "User_Not_Redirect_Home_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
					
			
		}
}
@AfterTest
public void aftertest() {
   getDriver().close();
}
}