package Execute;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import Master.Result_Email_Notification;

public class Run_Email_Result {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		TestListenerAdapter tla4 = new TestListenerAdapter();
		TestNG testng4 = new TestNG();
		testng4.setTestClasses(new Class[] { Result_Email_Notification.class });
		testng4.addListener(tla4);
		testng4.run();
		}
}