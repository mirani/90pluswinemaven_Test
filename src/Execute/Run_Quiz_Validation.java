package Execute;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import Runnable.Quiz_validation;

public class Run_Quiz_Validation {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { Quiz_validation.class });
		testng.addListener(tla);
		testng.run();
		}
}
