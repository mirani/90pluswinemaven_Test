package Execute;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import Runnable.register_complete;

public class Run_Register_Complete {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { register_complete.class });
		testng.addListener(tla);
		testng.run();
		}
}
