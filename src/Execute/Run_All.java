package Execute;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import General.contact_us;
import Login.Login_FP_Validation;
import Master.Final_Result;
import Master.Result_Email_Notification;
import Runnable.Quiz_validation;
import Runnable.Register_validation;
import Runnable.Shipment_Validation;
import Runnable.register_complete;

public class Run_All {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		
		TestListenerAdapter tla1 = new TestListenerAdapter();
		TestNG testng1 = new TestNG();
		testng1.setTestClasses(new Class[] { Register_validation.class });
		testng1.addListener(tla1);
		testng1.run();
		
		TestListenerAdapter tla2 = new TestListenerAdapter();
		TestNG testng2 = new TestNG();
		testng2.setTestClasses(new Class[] { Quiz_validation.class });
		testng2.addListener(tla2);
		testng2.run();
		
		TestListenerAdapter tla3 = new TestListenerAdapter();
		TestNG testng3 = new TestNG();
		testng3.setTestClasses(new Class[] { Shipment_Validation.class });
		testng3.addListener(tla3);
		testng3.run();
		
		TestListenerAdapter tla4 = new TestListenerAdapter();
		TestNG testng4 = new TestNG();
		testng4.setTestClasses(new Class[] { register_complete.class });
		testng4.addListener(tla4);
		testng4.run();
		
		TestListenerAdapter tla5 = new TestListenerAdapter();
		TestNG testng5 = new TestNG();
		testng5.setTestClasses(new Class[] { contact_us.class });
		testng5.addListener(tla5);
		testng5.run();
		
		TestListenerAdapter tla6 = new TestListenerAdapter();
		TestNG testng6 = new TestNG();
		testng6.setTestClasses(new Class[] { Final_Result.class });
		testng6.addListener(tla6);
		testng6.run();
		
		TestListenerAdapter tla7 = new TestListenerAdapter();
		TestNG testng7 = new TestNG();
		testng7.setTestClasses(new Class[] { Result_Email_Notification.class });
		testng7.addListener(tla7);
		testng7.run();
		
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { Login_FP_Validation.class });
		testng.addListener(tla);
		testng.run();
		}
}